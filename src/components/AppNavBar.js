import {Navbar, Nav} from 'react-bootstrap';

function AppNavBar(){
	return (
		<Navbar bg="light" expand="lg">
			<Navbar.Brand>
				Course Booking App B164
			</Navbar.Brand>
			<Navbar.Toggle aria-controls="basic-navbar-nav"/>
			<Navbar.Collapse id="basic-navbar-nav">
				<Nav className="ml-auto" variant="tabs">
					<Nav.Item>
						<Nav.Link>Register</Nav.Link>
					</Nav.Item>
					<Nav.Item>
						<Nav.Link>Login</Nav.Link>
					</Nav.Item>
					<Nav.Item>
						<Nav.Link>Courses</Nav.Link>
					</Nav.Item>
				</Nav>
			</Navbar.Collapse>
	
		</Navbar>
		)
};

export default AppNavBar;