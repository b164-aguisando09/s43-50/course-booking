

import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Card from 'react-bootstrap/Card';


export default function Highlights(){
	return(
		<Row className="px-5 pb-5">
			<Col className="py-2" xs={12} md={4}>
				<Card>
					<Card.Body>
						<Card.Title>Learn From Home</Card.Title>
						<Card.Text>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
							tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>
			<Col className="py-2" xs={12} md={4}>
				<Card>
					<Card.Body>
						<Card.Title>Learn From Home</Card.Title>
						<Card.Text>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
							tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>
			<Col className="py-2" xs={12} md={4}>
				<Card>
					<Card.Body>
						<Card.Title>Learn From Home</Card.Title>
						<Card.Text>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
							tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>
		</Row>

		);
};




